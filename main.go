package main

import (
	"embed"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/buger/jsonparser"
)

//used to embed the json file into binary
//go:embed js.json
var f embed.FS

func main() {

	http.HandleFunc("/api/", func(w http.ResponseWriter, r *http.Request) {
		uriSegments := strings.Split(r.URL.Path, "/")
		var placementid = uriSegments[2] //what index of the array
		var label = uriSegments[3]       // new data label
		var id = uriSegments[4]          //new data id
		//example url would be http://localhost:3001/api/0/cat/4
		//it adds "4":{"label":"cat","children":[] under the first array children
		//if you visit http://localhost:3001/api/root/value/id
		/*
			second segment will pick where the new json will be inserted
			third segement is its label
			fourth segment is its id
		*/

		//read the file embedded json file hence the "f"
		content, err := f.ReadFile("js.json")
		if err != nil {
			log.Fatal("Error when opening file: ", err)
		}
		var a any //need any type because its unstructured

		//decode the json into []bytes
		err = json.Unmarshal(content, &a)
		if err != nil {
			fmt.Print(err)
		}
		//search the api
		vv, err := jsonparser.Set(content, []byte(`{"`+id+`":{"label":"`+label+`","children":[]}}`), "[0]", "1", "children", "["+placementid+"]", "1", "children")
		if err != nil {
			fmt.Print(err)
		}
		Source := (*json.RawMessage)(&vv)
		output, err := json.Marshal(Source)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		w.Header().Set("content-type", "application/json")
		w.Write(output)
	})
	fmt.Printf("Starting server at port 3001\n")
	if err := http.ListenAndServe(":3001", nil); err != nil {
		log.Fatal(err)
	}

}

//to cross compile to whatever system you need https://freshman.tech/snippets/go/cross-compile-go-programs/
